const path = require("path");
const zip = require("zip-a-folder");
const package = require("./package");
var fs = require("fs");

const zipper = async (configVersion, req, res) => {
  const zipName = `${package.name}_${configVersion}.zip`;
  let src = path.resolve(__dirname, "./build");
  let dest = path.resolve(
    __dirname,
    "./out",
    zipName
  );
  await zip.zip(src, dest);
  
  const contents = fs.readFileSync(dest, { encoding: "base64" });

  return {
    configPath: zipName,
    data: contents,
  };
};

exports.zipper = zipper;
