const currencies = {
  rub: "₽",
  dol: "$",
  eur: "€",
};

export default currencies;
