import config from "../_config/_final.json";

export const sendData = async (data) => {
  console.log(data);
  // axios.post(config.api.to, { ...data }).then((res) => {
  //   console.log(res);
  //   console.log(res.data);
  // });

  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  myHeaders.append("Access-Control-Allow-Origin", "*");

  const raw = JSON.stringify({
    ...data,
  });
  console.log("sending data", raw);
  const requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  await fetch(config.api, requestOptions)
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
    })
    .catch((error) => console.log("error", error));
};
