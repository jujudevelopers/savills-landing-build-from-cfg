export const addItalictToText = (text, italic) => {
  let newText = text;

  italic.forEach((item) => {
    newText = newText.replace(item, `<em>${item}</em>`);
  });

  return newText;
};

export const phoneWithoutSymbols = (text) => {
  return text.replace(/[^0-9 ]| /g, "");
};

export const convertNumber = (value, seperator = "'") => {
  let val = value / 1;
  const sep =
    seperator === "'" ? "<span class='filter__apostrophe'>'</span>" : " ";
  const formed = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, sep);
  return formed;
};

export const hideScrollMain = (isHidden) => {
  if (isHidden) {
    document.documentElement.style.overflow = "hidden";
  } else {
    document.documentElement.style.overflow = "auto";
  }
};
