import React from "react";
import ReactDOM from "react-dom";
import "./assets/scss/styles.scss";
import App from "./containers/App";

// ReactDOM.render(
ReactDOM.hydrate(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
