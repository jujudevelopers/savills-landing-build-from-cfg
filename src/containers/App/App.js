import React, { Fragment, useMemo, useState } from "react";
import { uid } from "uid";
import About from "../../components/About";
import Apartments from "../../components/Apartments";
import Benefits from "../../components/Benefits";
import Contacts from "../../components/Contacts";
import Details from "../../components/Details";
import Facilities from "../../components/Facilities";
import Footer from "../../components/Footer";
import Gallery from "../../components/Gallery";
import Header from "../../components/Header";
import config from "../../_config/_final.json";
import PopupMainCarousel from "../../components/_common/PopupMainCarousel";
import PopupForm from "../../components/PopupForm";
import Location from "../../components/Location";

const App = () => {
  const [mainGalleryStartSlide, setMainGalleryStartSlide] = useState(null);
  const [type, setType] = useState(null);
  const [isFormSuccess, setFormSuccess] = useState(false);
  const components = {
    header: (data) => (
      <Header
        data={data}
        setType={setType}
        setMainGalleryStartSlide={setMainGalleryStartSlide}
      />
    ),
    about: (data) => <About data={data} />,
    gallery: (data) => (
      <Gallery
        data={data}
        setMainGalleryStartSlide={setMainGalleryStartSlide}
        setType={setType}
      />
    ),
    apartments: (data) => (
      <Apartments data={data} setFormSuccess={setFormSuccess} />
    ),
    facilities: (data) => <Facilities data={data} />,
    details: (data) => <Details data={data} setType={setType} />,
    benefits: (data) => <Benefits data={data} setType={setType} />,
    footer: (data) => <Footer data={data} />,
    contacts: (data) => <Contacts data={data} setType={setType} />,
    location: (data) => <Location data={data} />,
  };

  const getComponents = useMemo(() => {
    return config.order.map((item) => {
      const componentData = config.sections.find(
        (el) => el.id === item.id && el.type === item.type
      );
      return (
        <Fragment key={uid()}>{components[item.type](componentData)}</Fragment>
      );
    });
  }, [components]);

  const galleryData = config
    ? config.sections.find((el) => el.id === 1 && el.type === "gallery")
    : [];

  return (
    <div className={`app`}>
      {config && (
        <PopupForm
          type={type}
          setType={setType}
          isFormSuccess={isFormSuccess}
          setFormSuccess={setFormSuccess}
          data={config.forms}
        />
      )}
      <PopupMainCarousel
        data={galleryData.popupGallery}
        mainGalleryStartSlide={mainGalleryStartSlide}
        setMainGalleryStartSlide={setMainGalleryStartSlide}
      />
      {getComponents && getComponents}
    </div>
  );
};

export default App;
