import "./index.scss";

import React, { useEffect, useState } from "react";
import ApartmentItems from "./ApartmentItems";
import Filter from "./Filter";
import MoreButton from "../_common/MoreButton";
import ApItem from "./ApItem";
import $ from "jquery";

const Apartments = ({ data, setFormSuccess }) => {
  const [isAll, setAll] = useState(false);
  const [isFilterOpen, setFilterOpen] = useState(false);
  const [apItemOpened, setApItemOpened] = useState(null);
  const [filterData, setFilterData] = useState({
    currency: "rub",
    isPrice: false,
    isRent: false,
    area: [0, 10000000],
    bPrice: [0, 1000000000],
    bRent: [0, 1000000000],
    rooms: [0, 10],
  });

  const items = data ? data.items : [];

  const handleFilterTopFunctionality = () => {
    $(function () {
      $(window).scroll(function () {
        handleApartmentsTopToolbarChange();
      });
      $(window).resize(function () {
        handleApartmentsTopToolbarChange();
      });

      function handleApartmentsTopToolbarChange() {
        const apContainerWrapTop = $(".apartments__container-wrap").offset()
          .top;
        const apFilterTop = $(".apartments__filter").offset().top;
        const apContainerWrapHeight = $(".apartments__container-wrap").height();
        const apContainerWrapBottom =
          apContainerWrapTop + apContainerWrapHeight - 300;

        if (apContainerWrapHeight > 400) {
          if (
            $(window).scrollTop() >= apContainerWrapTop &&
            $(window).scrollTop() < apContainerWrapBottom
          ) {
            $(".apartments-top-filter").css({ display: "grid" });
          }

          if (
            $(window).scrollTop() <= apFilterTop ||
            $(window).scrollTop() > apContainerWrapBottom
          ) {
            $(".apartments-top-filter").css({ display: "none" });
          }
        } else {
          $(".apartments-top-filter").css({ display: "none" });
        }
      }
    });
  };

  useEffect(() => {
    handleFilterTopFunctionality();
  }, []);

  return data ? (
    <>
      <ApItem
        currency={filterData.currency}
        items={items}
        openedId={apItemOpened}
        setApItemOpened={setApItemOpened}
        setFormSuccess={setFormSuccess}
      />
      <Filter
        isOpen={isFilterOpen}
        setFilterOpen={setFilterOpen}
        setFilterData={setFilterData}
        filterData={filterData}
        data={data}
        setAll={setAll}
      />
      <div className="apartments-section">
        <section className="apartments">
          <div
            className="apartments-top-filter"
            onClick={() => setFilterOpen(true)}
          >
            <span className="apartments-top-filter__title">Фильтры</span>
            <div className="apartments-top-filter__icon">
              <img src={"/images/icons/switch.svg"} alt="switch" />
            </div>
          </div>
          <h2 className="apartments__title">
            Квартиры <br className="apartments__title-break" />
            <em className="apartments__title-2nd">в Елисейских Холмах</em>
          </h2>
          <div className="apartments__top">
            <p className="apartments__text">
              Представленные объекты тщательно отобраны и проверены нашими
              экспертами.
            </p>
            <div
              className="apartments__filter"
              onClick={() => setFilterOpen(true)}
            >
              <h3 className="apartments__filter-title">Фильтры</h3>
              <img src={"./images/icons/switch.svg"} alt="switch" />
            </div>
          </div>
          <div className="apartments__container-wrap">
            <div className="apartments__container" id="apartments-container">
              <ApartmentItems
                items={items}
                isAll={isAll}
                setApItemOpened={setApItemOpened}
                filterData={filterData}
              />
            </div>
          </div>
          <MoreButton
            title={"Все\nквартиры"}
            amount={"300+"}
            func={"300+"}
            type={"apartment"}
            isAll={isAll}
            setAll={setAll}
          />
        </section>
      </div>
    </>
  ) : null;
};

export default Apartments;
