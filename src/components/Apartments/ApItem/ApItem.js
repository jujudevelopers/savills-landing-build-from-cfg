import "./index.scss";

import React, { useEffect, useRef } from "react";
import { convertNumber, hideScrollMain } from "../../../helpers/functions";
import classNames from "classnames";
import ApCarousel from "../ApCarousel";
import { sendData } from "../../../helpers/api";
import currencies from "../../../helpers/currencies";

const ApItem = ({
  items,
  openedId,
  setApItemOpened,
  setFormSuccess,
  currency,
}) => {
  const inputRef = useRef();

  useEffect(() => {
    hideScrollMain(openedId);
  }, [openedId]);

  const AIClass = classNames({
    "apartment-item": true,
    "apartment-item--active": openedId !== null,
  });

  const item = items.find((el) => el.id === openedId);

  const handleBooking = () => {
    setFormSuccess(true);
    const phone = inputRef.current.value;
    const sendingData = {
      apartmentId: openedId,
      number: phone,
      priceType: "book",
    };
    sendData(sendingData);
    inputRef.current.value = null;
  };

  return (
    <div className={AIClass}>
      {item && (
        <>
          <div className="apartment-item__top">
            <img
              src="./images/icons/close-black.svg"
              alt="close"
              className="apartment-item__close"
              onClick={() => setApItemOpened(null)}
            />
          </div>
          <div className="apartment-item__container">
            <ApCarousel item={item} />
            <div className="apartment-item-descr">
              <img
                src="./images/icons/arrow-down-silver.svg"
                alt="arrow"
                className="apartment-item__arrow"
              />
              <h2 className="apartment-item-descr__title">
                {item.customType ? (
                  item.customType
                ) : (
                  <>
                    <span>{item.rooms}</span>
                    -комнатная
                    <br />
                    <em>квартира</em>
                  </>
                )}
              </h2>
              <div className="apartment-item-descr__list">
                <div className="apartment-item-descr__item">
                  <span className="apartment-item-descr__number">
                    {item.floor}
                  </span>
                  <p className="apartment-item-descr__text">Этаж</p>
                </div>
                <div className="apartment-item-descr__item">
                  <span className="apartment-item-descr__number">
                    {item.housing}
                  </span>
                  <p className="apartment-item-descr__text">корпус</p>
                </div>
                <div className="apartment-item-descr__item">
                  <span className="apartment-item-descr__number">
                    <span id="apartment-area">{item.area}</span> м<sup>2</sup>
                  </span>
                  <p className="apartment-item-descr__text">площадь</p>
                </div>
                <div className="apartment-item-descr__item">
                  <span
                    className="apartment-item-descr__number"
                    id="apartment-bathrooms"
                  >
                    {item.bathrooms}
                  </span>
                  <p className="apartment-item-descr__text">санузла</p>
                </div>
              </div>
              {item.priceType === "sold" && (
                <div className="apartment-item-descr__status">Продана</div>
              )}
              {item.priceType !== "sold" && (
                <div className="apartment-item__form">
                  <div className="apartment-item-descr__price">
                    {convertNumber(item.price[currency], " ")}{" "}
                    {currencies[currency]}
                  </div>
                  <input
                    type="text"
                    ref={inputRef}
                    className={`custom-input apartment-item-descr__input`}
                    placeholder={"+7 912 345-67-89"}
                  />
                  <button
                    type="button"
                    className="custom-button apartment-item-descr__button"
                    onClick={handleBooking}
                  >
                    забронировать квартиру
                  </button>
                  <p className="apartment-item-descr__text-bottom">
                    Бронируя, вы даёте согласие <br />
                    на обработку персональных данных.
                  </p>
                </div>
              )}
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default ApItem;
