import "./index.scss";

import React, { useEffect, useRef } from "react";
import { uid } from "uid";
import classNames from "classnames";
import { Splide, SplideSlide } from "@splidejs/react-splide";

const ApCarousel = ({ item }) => {
  const primaryRef = useRef(null);
  const secondaryRef = useRef(null);

  const images = {
    items: item ? item.images : [],
    centeredImages: item ? item.centeredImages : [],
  };

  useEffect(() => {
    primaryRef.current.sync(secondaryRef.current.splide);
  }, []);

  const ACClass = classNames({
    apcarousel: true,
  });

  const primaryOptions = {
    type: "loop",
    width: 800,
    perPage: 1,
    perMove: 1,
    gap: 0,
    arrows: false,
    pagination: false,
  };

  const secondaryOptions = {
    rewind: true,
    width: 800,
    gap: 16,
    cover: true,
    focus: "center",
    isNavigation: true,
    updateOnMove: true,
    fixedWidth: 90,
    fixedHeight: 90,
    arrows: false,
    pagination: false,
  };

  return (
    <div className={ACClass}>
      <div className={`apcarousel__slider--primary`}>
        <Splide options={primaryOptions} ref={primaryRef}>
          {images.items.map((el) => {
            const AIClass = classNames({
              apcarousel__image: true,
              "apcarousel__image--full": !images.centeredImages.includes(el),
              "apcarousel__image--center": images.centeredImages.includes(el),
            });
            return (
              <SplideSlide key={uid()}>
                <img
                  className={AIClass}
                  src={`./images/apartments/popup/${item.id}/${el}.jpg`}
                  alt={el}
                />
              </SplideSlide>
            );
          })}
        </Splide>
      </div>
      <div className={`apcarousel__slider--secondary`}>
        <Splide options={secondaryOptions} ref={secondaryRef}>
          {images.items.map((el) => {
            const AIClass = classNames({
              apcarousel__image: true,
              "apcarousel__image--full": !images.centeredImages.includes(el),
              "apcarousel__image--center": images.centeredImages.includes(el),
            });
            return (
              <SplideSlide key={uid()}>
                <img
                  className={AIClass}
                  src={`./images/apartments/popup/${item.id}/${el}.jpg`}
                  alt={el}
                />
              </SplideSlide>
            );
          })}
        </Splide>
      </div>
    </div>
  );
};

export default ApCarousel;
