import React from "react";
import classNames from "classnames";
import { uid } from "uid";

const FilterToolbar = ({ currency, setCurrency }) => {
  const options = [
    {
      value: "rub",
      icon: "₽",
    },
    {
      value: "dol",
      icon: "$",
    },
    {
      value: "eur",
      icon: "€",
    },
  ];

  return (
    <div key={uid()} className="filter-toolbar">
      {options.map((item) => {
        const ftC = classNames({
          "filter-toolbar__item": true,
          "filter-toolbar__item--active": currency === item.value,
        });
        return (
          <div
            key={uid()}
            className={ftC}
            onClick={() => setCurrency(item.value)}
          >
            {item.icon}
          </div>
        );
      })}
    </div>
  );
};

export default FilterToolbar;
