import "./index.scss";

import React, { useEffect, useState } from "react";
import { convertNumber, hideScrollMain } from "../../../helpers/functions";
import classNames from "classnames";
import FilterToolbar from "./FilterToolbar";
import { Slider, Switch } from "antd";

const Filter = ({
  isOpen,
  setFilterOpen,
  filterData,
  setFilterData,
  data,
  setAll,
}) => {
  const [filterMinMaxData, setMinMaxFilterData] = useState({
    currency: "rub",
    isPrice: false,
    isRent: false,
    area: [0, 10000000],
    bPrice: [0, 1000000000],
    bRent: [0, 1000000000],
    rooms: [0, 10],
  });
  const [isPrice, setIsPrice] = useState(false);
  const [isRent, setIsRent] = useState(false);
  const [area, setArea] = useState(filterMinMaxData.area);
  const [bRent, setBRent] = useState(filterMinMaxData.bRent);
  const [bPrice, setBPrice] = useState(filterMinMaxData.bPrice);
  const [rooms, setRooms] = useState(filterMinMaxData.rooms);
  const [currency, setCurrency] = useState("rub");

  useEffect(()=> {
    setBRent(filterMinMaxData.bRent)
    setBPrice(filterMinMaxData.bPrice)
  },[isRent, isPrice])
  const formFilterData = (currencyArg) => {
    const newFilterData = data.items;
    const allArea = newFilterData.map((item) => item.area);
    const allBPrice = newFilterData
      .map((item) => {
        if (item.type === "price") {
          return item.price[currencyArg];
        }
        return null
      })
      .filter((item) => item);
    const allBRent = newFilterData
      .map((item) => {
        if (item.type === "rent") {
          return item.price[currencyArg];
        }
        return null
      })
      .filter((item) => item);
    const allRooms = newFilterData.map((item) => item.rooms);
    const prepData = {
      area: [Math.min(...allArea), Math.max(...allArea)],
      bPrice: [Math.min(...allBPrice), Math.max(...allBPrice)],
      bRent: [Math.min(...allBRent), Math.max(...allBRent)],
      rooms: [0, Math.max(...allRooms)],
    };

    setArea(prepData.area);
    setBPrice(prepData.bPrice);
    setBRent(prepData.bRent);

    setRooms(prepData.rooms);
    setMinMaxFilterData(prepData);

    return prepData;
  };

  useEffect(() => {
    if (data) {
      formFilterData(currency);
    }
  }, [data, currency]);

  useEffect(() => {
    hideScrollMain(isOpen);
  }, [isOpen]);

  useEffect(() => {
    setIsRent(filterData.isRent);
    setIsPrice(filterData.isPrice);
    setArea(filterData.area);
    setBRent(filterData.bRent);
    setBPrice(filterData.bPrice);
    setRooms(filterData.rooms);
    setCurrency(filterData.currency);
  }, [filterData]);

  const filterClasses = classNames({
    filter: true,
    "filter--active": isOpen,
  });

  const handleReset = () => {
    setCurrency("rub");
    const prepdata = formFilterData("rub");

    setIsRent(false);
    setIsPrice(false);
    console.log(filterMinMaxData);

    setFilterData({
      currency: "rub",
      isPrice: false,
      isRent: false,
      area: prepdata.area,
      bRent: prepdata.bRent,
      bPrice: prepdata.bPrice,
      rooms: prepdata.rooms,
    });
  };

  const handleSubmit = () => {
    const sendData = {
      currency,
      isPrice,
      isRent,
      area,
      bRent,
      bPrice,
      rooms,
    };

    setAll(true);
    setFilterData(sendData);
  };

  const marks = {
    0: "",
    1: "",
    2: "",
    3: "",
    4: "",
    5: "",
    6: "",
  };
  return (
    <div className={filterClasses}>
      <div className="filter__wrap" onClick={() => setFilterOpen(false)}></div>
      <div className="filter__container">
        <div className="filter__bg"></div>
        <div className="filter-top">
          <span className="filter__title filter__title--white">Фильтры</span>
          <img
            src="./images/icons/close-white.svg"
            alt="close"
            className="filter__close"
            onClick={() => setFilterOpen(false)}
          />
          <div className="filter-top__bg"></div>
        </div>
        <div className="filter-body">
          <div className="filter-body-top">
            <div className="filter-body-top__item">
              <span className="filter__title filter__title--black">
                Продажа
              </span>
              <Switch
                size="large"
                onChange={(e) => setIsPrice(e)}
                checked={isPrice}
              />
            </div>
            <div className="filter-body-top__item">
              <span className="filter__title filter__title--black">Аренда</span>
              <Switch
                size="large"
                onChange={(e) => setIsRent(e)}
                checked={isRent}
              />
            </div>
          </div>
          <FilterToolbar currency={currency} setCurrency={setCurrency} />
          <div className="filter-bottom">
            <div className="filter-bottom__item filter-bottom__item--area">
              <div className="filter-bottom__top">
                <p>
                  Площадь, м<sup>2</sup>
                </p>
                <p id="square-range-amount">
                  <span
                    dangerouslySetInnerHTML={{
                      __html: area[0],
                    }}
                  />{" "}
                  —{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: area[1],
                    }}
                  />
                </p>
              </div>
              <div id="square-range"></div>
              <Slider
                range
                min={filterMinMaxData.area[0]}
                max={filterMinMaxData.area[1]}
                defaultValue={area}
                onChange={(value) => {
                  setArea(value);
                }}
                value={area}
              />
            </div>
            {((!isRent && isPrice) ||
              (!isRent && !isPrice) ||
              (isRent && isPrice)) && (
              <div className="filter-bottom__item filter-bottom__item--budget">
                <div className="filter-bottom__top">
                  <p>бюджет, ₽</p>
                  <p id="budget-range-amount">
                    <span
                      dangerouslySetInnerHTML={{
                        __html: convertNumber(bPrice[0]),
                      }}
                    />{" "}
                    —{" "}
                    <span
                      dangerouslySetInnerHTML={{
                        __html: convertNumber(bPrice[1]),
                      }}
                    />
                  </p>
                </div>
                <div id="budget-range"></div>
                <Slider
                  range
                  min={filterMinMaxData.bPrice[0]}
                  max={filterMinMaxData.bPrice[1]}
                  defaultValue={bPrice}
                  onChange={(value) => {
                    setBPrice(value);
                  }}
                  value={bPrice}
                />
              </div>
            )}
            {((isRent && !isPrice) ||
              (!isRent && !isPrice) ||
              (isRent && isPrice)) && (
              <div className="filter-bottom__item filter-bottom__item--budget">
                <div className="filter-bottom__top">
                  <p>бюджет, ₽</p>
                  <p id="budget-range-amount">
                    <span
                      dangerouslySetInnerHTML={{
                        __html: convertNumber(bRent[0]),
                      }}
                    />{" "}
                    —{" "}
                    <span
                      dangerouslySetInnerHTML={{
                        __html: convertNumber(bRent[1]),
                      }}
                    />
                  </p>
                </div>
                <div id="budget-range"></div>
                <Slider
                  range
                  min={filterMinMaxData.bRent[0]}
                  max={filterMinMaxData.bRent[1]}
                  defaultValue={bRent}
                  onChange={(value) => {
                    setBRent(value);
                  }}
                  value={bRent}
                />
              </div>
            )}

            <div className="filter-bottom__item filter-bottom__item--rooms">
              <div className="filter-bottom__top">
                <p>Количество комнат</p>
                <p id="rooms-range-amount">
                  {" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: convertNumber(rooms[0]),
                    }}
                  />{" "}
                  —{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: convertNumber(rooms[1]),
                    }}
                  />
                </p>
              </div>
              <div id="rooms-range"></div>
              <Slider
                range
                marks={marks}
                step={1}
                min={filterMinMaxData.rooms[0]}
                max={filterMinMaxData.rooms[1]}
                onChange={(value) => {
                  setRooms(value);
                }}
                value={rooms}
              />
            </div>
          </div>
          <div className="filter-buttons">
            <button
              type="button"
              className="custom-button filter__button filter__button--transparent"
              onClick={handleReset}
            >
              Сбросить все
            </button>
            <button
              type="button"
              className="custom-button filter__button filter__button--yellow"
              onClick={handleSubmit}
            >
              применить фильтры
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Filter;
