import "./index.scss";

import React, { useState, useEffect } from "react";
import { uid } from "uid";
import { convertNumber } from "../../../helpers/functions";
import $ from "jquery";

const ApartmentItems = ({ items, isAll, setApItemOpened, filterData }) => {
  const [amount, setAmount] = useState(3);

  const handleResize = () => {
    const windowWidth = window.innerWidth;
    const init = windowWidth < 1000 && windowWidth >= 768 ? 2 : 3;
    setAmount(init);
  };
  useEffect(() => {
    handleResize();
    $(window).resize(function () {
      handleResize();
    });
  }, []);

  return (
    <>
      {items
        .filter((item) => {
          const areaCondition =
            filterData.area[0] <= item.area && filterData.area[1] >= item.area;
          let roomsCondition = true;
          let priceCondition = true;
          if (areaCondition) {
            roomsCondition =
              filterData.rooms[0] <= item.rooms &&
              filterData.rooms[1] >= item.rooms;
          }

          if (priceCondition) {
            priceCondition =
              item.priceType === "sold"
                ? true
                : !filterData.isPrice &&
                  !filterData.isRent &&
                  filterData.bPrice[0] === item.price[filterData.currency] &&
                  filterData.bPrice[1] === item.price[filterData.currency] &&
                  filterData.bRent[0] === item.price[filterData.currency] &&
                  filterData.bRent[1] === item.price[filterData.currency]
                ? true
                : item.priceType === "price"
                ? filterData.bPrice[0] <= item.price[filterData.currency] &&
                  filterData.bPrice[1] >= item.price[filterData.currency]
                : item.priceType === "rent"
                ? filterData.bRent[0] <= item.price[filterData.currency] &&
                  filterData.bRent[1] >= item.price[filterData.currency]
                : false;
          }

          let PriceSwitch = true;

          if (filterData.isPrice || filterData.isRent) {
            console.log(filterData.isPrice, item.priceType);
            PriceSwitch =
              (filterData.isPrice && item.priceType === "price") ||
              (filterData.isRent && item.priceType === "rent") ||
              item.priceType === "sold"
                ? true
                : !filterData.isPrice && !filterData.isRent
                ? true
                : false;
          }

          return (
            areaCondition && roomsCondition && priceCondition && PriceSwitch
          );
        })
        .filter((item, index) => {
          return !isAll ? index < amount : true;
        })
        .map((item) => {
          const currencies = {
            rub: "₽",
            dol: "$",
            eur: "€",
          };

          const labelTypeText =
            item.priceType === "price"
              ? `${convertNumber(item.price[filterData.currency])} ${
                  currencies[filterData.currency]
                }`
              : item.priceType === "rent"
              ? `${convertNumber(item.price[filterData.currency])} ${
                  currencies[filterData.currency]
                } / мес`
              : "Продана";
          const roomsTitle = item.customType
            ? item.customType
            : item.rooms > 1
            ? `${item.rooms} комнаты`
            : "1 комнатa";

          return (
            <article key={uid()} className="apartments__item">
              <div className="apartments__item-top">
                <div className="apartments__item-left">
                  <span className="apartments__item-meters">
                    {item.area} м<sup>2</sup>
                  </span>
                  <span className="apartments__item-floor">
                    {item.floor} этаж
                  </span>
                </div>
                <span className="apartments__item-rooms">{roomsTitle}</span>
              </div>
              <div
                className="apartments__item-wrap"
                data-id={item.id}
                onClick={() => setApItemOpened(item.id)}
              >
                <img
                  src={`./images/apartments/${item.id}.jpg`}
                  alt="switch"
                  className="apartments__item-image"
                />
                <div
                  className={`apartments__label apartments__label--${item.priceType}`}
                  dangerouslySetInnerHTML={{
                    __html: labelTypeText,
                  }}
                />
              </div>
            </article>
          );
        })}
    </>
  );
};

export default ApartmentItems;
