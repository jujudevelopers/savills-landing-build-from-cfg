import "./index.scss";

import React from "react";
import { addItalictToText } from "../../helpers/functions";
import { uid } from "uid";
import CustomButton from "../_common/CustomButton";

const Benefits = ({ data, setType }) => {
  const images = {
    ecology: "./images/icons/ecology.svg",
    pin: "./images/icons/pin.svg",
    school: "./images/icons/school.svg",
    money: "./images/icons/money.svg",
    "swimming pool": "./images/icons/swimming-pool.svg",
  };
  return (
    <div className="benefits-section">
      <section className="benefits">
        <h2
          className="benefits__title"
          dangerouslySetInnerHTML={{
            __html: addItalictToText(data.title.text, data.title.italic),
          }}
        />
        <div className="benefits__list">
          {data.items.map((item) => (
            <div key={uid()} className="benefits__item">
              <img
                src={images[item.icon]}
                alt="pin"
                className="benefits__icon"
              />
              <p className="benefits__text">{item.text}</p>
            </div>
          ))}
        </div>
        {data.button.active && (
          <CustomButton
            title={data.button.title}
            classes="benefits__button"
            func={() => setType(data.button.type)}
          />
        )}
      </section>
    </div>
  );
};

export default Benefits;
