import "./index.scss";

import React from "react";
import { addItalictToText, phoneWithoutSymbols } from "../../helpers/functions";

const Contacts = ({ data, setType }) => {
  return (
    <div className="contacts-section">
      <section className="contacts">
        <div className="contacts__left">
          <h2 className="contacts__title">{data.left.title}</h2>
          <img
            src={"./images/contacts/manager.jpg"}
            alt="manager"
            className="contacts__image"
          />
          <p className="contacts__name">Михаил Гаврильчук</p>
          <p className="contacts__occupation">Продажа городской недвижимости</p>
          <a
            href={`tel:+${phoneWithoutSymbols(data.left.phone)}`}
            className="contacts__phone"
          >
            {data.left.phone}
          </a>
          <div className="contacts__left-border contacts__left-border--top"></div>
          <div className="contacts__left-border contacts__left-border--bottom"></div>
        </div>
        <div className="contacts__right">
          <div className="contacts__right-container">
            <h2
              className="contacts__title--right"
              dangerouslySetInnerHTML={{
                __html: addItalictToText(
                  data.right.title.text,
                  data.right.title.italic
                ),
              }}
            />
            <p className="contacts__text">{data.right.text}</p>
            {data.right.button.active && (
              <p
                className="contacts__link"
                id="contacts__button--call"
              >
                <span onClick={() => setType(data.right.button.type)}>
                  {data.right.button.text}
                </span>
              </p>
            )}
            <a
              href={`tel:+${phoneWithoutSymbols(data.right.phone)}`}
              className="contacts__tel"
            >
              {data.right.phone}
            </a>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Contacts;
