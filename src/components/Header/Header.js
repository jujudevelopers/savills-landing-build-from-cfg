import "./index.scss";

import React, { useEffect } from "react";
import $ from "jquery";
import Slideshow from "./Slideshow";
import { phoneWithoutSymbols } from "../../helpers/functions";

const Header = ({
  setMainGalleryStartSlide,
  setType,
  data,
}) => {
  useEffect(() => {
    let currentText = 0;
    const texts = data ? data.runningText : ["", ""];

    setInterval(() => {
      $(".header__text-bg").css({
        left: "auto",
        right: 0,
        width: "0%",
      });
      $("#header-text").text(texts[currentText]);

      if (texts.length - 1 === currentText) {
        currentText = 0;
      } else {
        currentText++;
      }

      setTimeout(() => {
        $(".header__text-bg").css({
          right: "auto",
          left: 0,
          width: "100%",
        });
      }, 4700);
    }, 5000);
  }, [data]);

  return (
    <div
      className="header-section"
      style={{ backgroundColor: data.backgroundColor }}
    >
      <header className="header">
        <p className="header__text header__text--amount">
          <span id="header-slide-current">1</span> &mdash; 6
        </p>
        <a
          href={`tel:+${phoneWithoutSymbols(data.phone)}`}
          className="header__phone"
        >
          {data.phone}
        </a>
        <div className="header__text header__text--sell">
          <div
            className="header__text-bg"
            style={{ backgroundColor: data.backgroundColor }}
          />
          <div id="header-text">{data.runningText[0]}</div>
        </div>
        <img
          src={"./images/icons/arrow.svg"}
          alt="arrow"
          className="header__arrow"
        />
        <a href="tel:+79121234567">
          <div className="header__tel">
            <img
              src={"./images/icons/phone.svg"}
              alt="arrow"
              className="header__tel-icon"
            />
          </div>
        </a>
        <Slideshow
          setMainGalleryStartSlide={setMainGalleryStartSlide}
          setType={setType}
          data={data}
        />
      </header>
    </div>
  );
};

export default Header;
