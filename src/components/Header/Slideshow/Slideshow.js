import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import $ from "jquery";
import CustomButton from "../../_common/CustomButton";
import { addItalictToText } from "../../../helpers/functions";
import { uid } from "uid";

export default class Slideshow extends Component {
  state = {
    slideIndex: 0,
    updateCount: 0,
  };

  render() {
    const { setMainGalleryStartSlide, setType, data } = this.props;

    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      speed: 500,
      autoplaySpeed: 4000,
      cssEase: "linear",
      afterChange: (current, next) => {
        $("#header-slide-current").text(current + 1);
      },
    };
    const headerItems = data.slides.map((item, index) => {
      if (index === 0 || index === data.slides.length - 1) return null;
      return (
        <div className="header__item" key={uid()}>
          <div
            className={`header__bg ${
              item.border === "all"
                ? "header__bg--border-all"
                : item.border === "top"
                ? "header__bg--border-top"
                : ""
            }`}
            style={{
              backgroundImage: `url(./images/header/${item.image}.jpg)`,
            }}
          />
        </div>
      );
    });

    const firstItem = data.slides[0];
    const lastItem = data.slides[data.slides.length - 1];
    return (
      <Slider {...settings}>
        <div className="header__item">
          <div
            className="header__bg"
            style={{
              backgroundImage: `url(./images/header/${firstItem.image}.jpg)`,
            }}
          >
            <div className="header__block">
              <h1
                className="header__title"
                dangerouslySetInnerHTML={{
                  __html: addItalictToText(
                    firstItem.block.title.text,
                    firstItem.block.title.italic
                  ),
                }}
              />
              <div className="header__subtitle">{firstItem.block.text}</div>
            </div>
          </div>
        </div>
        {headerItems}
        <div className={`header__item`}>
          <div className="header__bg header__bg--6">
            <img
              src={`./images/icons/${lastItem.icon}.svg`}
              alt="grid"
              className="header__icon-grid"
            />
            <div className="header__buttons">
              {lastItem.buttons.gallery.active && (
                <CustomButton
                  title={lastItem.buttons.gallery.title}
                  classes="header__button header__button--transparent"
                  func={() => setMainGalleryStartSlide(0)}
                />
              )}
              {lastItem.buttons.button.active && (
                <CustomButton
                  title={lastItem.buttons.button.title}
                  classes="header__button header__button"
                  func={() => setType(lastItem.buttons.button.type)}
                />
              )}
            </div>
          </div>
        </div>
      </Slider>
    );
  }
}
