import React from "react";

const FacilitiesPopupItem = ({ item }) => {
  return (
    <article
      className="popup-facilities__item"
      style={{ borderColor: item.color }}
    >
      <div
        className="popup-facilities__image"
        style={{
          backgroundColor: item.color,
          mask: `url(./images/icons/${item.icon}.svg) no-repeat center / contain`,
          WebkitMask: `url(./images/icons/${item.icon}.svg) no-repeat center / contain`,
        }}
      />
      <h3 className="popup-facilities__text">{item.title}</h3>
      <p className="popup-facilities__subtext">{item.text}</p>
    </article>
  );
};

export default FacilitiesPopupItem;
