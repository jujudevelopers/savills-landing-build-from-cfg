import "./index.scss";

import React, { useEffect } from "react";
import { uid } from "uid";
import FacilitiesPopupItem from "./FacilitiesPopupItem";
import classNames from "classnames";
import { hideScrollMain } from "../../../helpers/functions";

const FacilitiesPopup = ({ items, isPopupOpen, setPopupOpen }) => {
  const PFClass = classNames({
    "popup-facilities": true,
    "popup-facilities--active": isPopupOpen,
  });

  useEffect(() => {
    hideScrollMain(isPopupOpen);
  }, [isPopupOpen]);

  const renderItems =
    items &&
    isPopupOpen &&
    items.map((item, i) => <FacilitiesPopupItem key={uid()} item={item} />);
  return (
    <div className={PFClass}>
      <img
        src="./images/icons/close-black.svg"
        alt="arrow"
        className="popup-facilities__close"
        onClick={() => setPopupOpen(false)}
      />
      <div className="popup-facilities__wrap">{renderItems}</div>
    </div>
  );
};

export default FacilitiesPopup;
