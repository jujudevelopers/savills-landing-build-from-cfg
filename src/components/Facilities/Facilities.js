import "./index.scss";

import React, { useEffect, useState } from "react";
import FacilitiesItem from "./FacilitiesItem";
import FacilitiesPopup from "./FacilitiesPopup";
import { uid } from "uid";
import $ from "jquery";
import ScrollContainer from "react-indiana-drag-scroll";
import classNames from "classnames";

const Facilities = ({ data }) => {
  const [isPopupOpen, setPopupOpen] = useState(false);
  const [initItems, setInitItems] = useState([]);
  const [isContainerCentered, setContainerCentered] = useState(false);

  const prepItems = () => {
    let items =
      data &&
      data.itemsMini.map((item, index) => {
        return index === 0 ? (
          <div key={uid()} className="facilities__item-first">
            <FacilitiesItem item={item} />
          </div>
        ) : (
          <FacilitiesItem key={uid()} item={item} />
        );
      });

    const lastItem = (
      <div key={uid()} className="facilities__item-last">
        <article
          className="facilities__item facilities__item--center facilities__item--last"
          style={{ borderColor: data.finalItemMini.color }}
          id="popup-facilities-open"
          onClick={() => setPopupOpen(true)}
        >
          <div
            className="facilities__image facilities__image--last"
            style={{
              backgroundColor: data.finalItemMini.color,
              mask: `url(./images/icons/${data.finalItemMini.icon}.svg) no-repeat center / contain`,
              WebkitMask: `url(./images/icons/${data.finalItemMini.icon}.svg) no-repeat center / contain`,
            }}
          />
          <h3 className="facilities__text facilities__text--center">
            {data.finalItemMini.title}
          </h3>
        </article>
      </div>
    );
    const allItems = [...items, lastItem];
    setInitItems(allItems);
  };

  useEffect(() => {
    const handleResize = () => {
      const windowWidth = window.innerWidth;
      const children = $(".facilities__container").children().length;
      const isCentered = windowWidth < 245 * children - 16 ? false : true;

      setContainerCentered(isCentered);
    };

    prepItems();
    handleResize();
    $(window).resize(function () {
      handleResize();
    });
  }, []);

  const containerClasses = classNames({
    facilities__container: true,
    "facilities__container--center": isContainerCentered,
  });

  return (
    <>
      <FacilitiesPopup
        items={data.itemsPopup}
        isPopupOpen={isPopupOpen}
        setPopupOpen={setPopupOpen}
      />
      <div className="facilities-section">
        <section className="facilities">
          <h2 className="facilities__title">{data.title}</h2>
          <div className="facilities__block" />
          <div
            className="facilities__wrapper"
            style={{
              height: "100%",
              width: "100%",
              transition: "transform 1s linear",
            }}
          >
            <ScrollContainer
              className="scroll-container attendance-scroller"
              hideScrollbars={true}
              nativeMobileScroll={true}
            >
              <div
                className={containerClasses}
                style={{
                  width: isContainerCentered
                    ? "100vw"
                    : `${250 * initItems.length}px`,
                }}
              >
                {initItems}
              </div>
            </ScrollContainer>
          </div>
        </section>
      </div>
    </>
  );
};

export default Facilities;
