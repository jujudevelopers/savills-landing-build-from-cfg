import React from "react";

const FacilitiesItem = ({ item, isFull }) => {
  return (
    <article className="facilities__item" style={{ borderColor: item.color }}>
      <div
        className="facilities__image"
        style={{
          backgroundColor: item.color,
          mask: `url(./images/icons/${item.icon}.svg) no-repeat center / contain`,
          WebkitMask: `url(./images/icons/${item.icon}.svg) no-repeat center / contain`,
        }}
      />
      <h3 className="facilities__text">{item.title}</h3>
      {isFull && (
        <p className="popup-facilities__subtext">
          Приточно-вытяжная вентиляция. Мультизональная система центрального
          кондиционирования типа VRF.
        </p>
      )}
    </article>
  );
};

export default FacilitiesItem;
