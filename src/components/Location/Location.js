import "./index.scss";

import React from "react";
import {
  YMaps,
  Map,
  ZoomControl,
  FullscreenControl,
  Placemark,
} from "react-yandex-maps";

const Location = ({ data }) => {
  return (
    <div className="location-seciton">
      <section className="location">
        <h2 className="location__title">Местоположение</h2>
        <div
          className={`map`}
          style={{ backgroundImage: `url("./images/location/map.jpg")` }}
        >
          <YMaps>
            <Map
              width="100%"
              height="100%"
              defaultState={{
                center: data.coordinates,
                controls: [],
                zoom: 16,
              }}
            >
              <FullscreenControl />
              <ZoomControl options={{ float: "right" }} />
              <Placemark
                geometry={data.coordinates}
                options={{
                  iconLayout: "default#image",
                  iconImageHref: "/images/icons/pin-map.svg",
                  iconImageSize: [32, 32],
                }}
              />
            </Map>
          </YMaps>
        </div>
      </section>
    </div>
  );
};

export default Location;
