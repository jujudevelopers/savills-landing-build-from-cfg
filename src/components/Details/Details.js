import "./index.scss";

import React from "react";
import { addItalictToText } from "../../helpers/functions";
import { uid } from "uid";
import CustomButton from "../_common/CustomButton";

const Details = ({ data, setType }) => {
  const items = data.items.map((item) => (
    <div key={uid()} className={`details__item details__item--${item.id}`}>
      <p className="details__number">{item.num}</p>
      <p className="details__content">{item.text}</p>
    </div>
  ));
  return (
    <div className="details-section">
      <section className="details">
        <h2
          className="details__title"
          dangerouslySetInnerHTML={{
            __html: addItalictToText(data.title.text, data.title.italic),
          }}
        />
        <p className="details__text">{data.text}</p>
        <div className="details__container">{items}</div>
        {data.button.active && (
          <CustomButton
            title={data.button.title}
            classes="details__button"
            func={() => setType(data.button.type)}
          />
        )}
      </section>
    </div>
  );
};

export default Details;
