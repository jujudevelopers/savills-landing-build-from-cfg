export default {
  excursion: {
    title: {
      text: "Записаться на экскурсию",
      italic: ["на экскурсию"],
    },
    inputs: ["tel"],
    textUnder: false,
    toolbar: false,
    button: "Записаться",
  },
  book: {
    title: {
      text: "Забронировать квартиру",
      italic: ["квартиру"],
    },
    textUnder: true,
    inputs: ["tel"],
    toolbar: false,
    contactBackText: "Перезвоним в течение получаса.",
    button: "забронировать",
  },
  callMe: {
    title: {
      text: "Перезвоните мне",
      italic: ["мне"],
    },
    textUnder: true,
    inputs: ["tel"],
    toolbar: false,
    button: "готово",
  },
  request1: {
    type: "request1",
    title: {
      text: "Оставить заявку",
      italic: ["заявку"],
    },
    textUnder: true,
    inputs: ["tel"],
    toolbar: false,
    button: "готово",
  },
  callback: {
    title: {
      text: "Обратный звонок",
      italic: ["звонок"],
    },
    textUnder: true,
    inputs: ["tel"],
    toolbar: false,
    button: "готово",
  },
  brochure1: {
    title: {
      text: "Скачать брошюру",
      italic: ["брошюру"],
    },
    textUnder: true,
    inputs: ["email"],
    toolbar: false,
    button: "готово",
  },
  request2: {
    title: {
      text: "Оставить заявку",
      italic: ["заявку"],
    },
    textUnder: false,
    inputs: ["tel", "email"],
    toolbar: false,
    button: "готово",
  },
  brochure2: {
    title: {
      text: "Скачать брошюру",
      italic: ["брошюру"],
    },
    textUnder: false,
    inputs: ["tel"],
    toolbar: true,
    button: "готово",
  },
};
