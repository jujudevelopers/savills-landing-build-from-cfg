import React from "react";
import { addItalictToText } from "../../../helpers/functions";

const Success = ({ additional }) => {
  return (
    <div className="request__success">
      <h3
        className="request__text request__text--success"
        dangerouslySetInnerHTML={{
          __html: addItalictToText(
            additional.success.title.text,
            additional.success.title.italic
          ),
        }}
      />
      <p className="request__subtext">{additional.success.contactBack}</p>
    </div>
  );
};

export default Success;
