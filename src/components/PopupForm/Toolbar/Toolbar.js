import classNames from "classnames";
import React from "react";
import { uid } from "uid";

const Toolbar = ({ toolbarOption, setToolbarOption }) => {
  const options = ["messages", "whatsapp", "telegram"];
  return (
    <ul className="request-toolbar">
      {options.map((item) => {
        const classes = classNames({
          "request-toolbar__item": true,
          "request-toolbar__item--active": item === toolbarOption,
        });
        return (
          <li
            key={uid()}
            className={classes}
            onClick={() => setToolbarOption(item)}
          >
            <img
              src={`./images/icons/${item}.svg`}
              alt={item}
              className="request-toolbar__image"
            />
          </li>
        );
      })}
    </ul>
  );
};

export default Toolbar;
