import "./index.scss";

import React, { useEffect, useMemo, useRef, useState } from "react";
import classNames from "classnames";
import { addItalictToText, hideScrollMain } from "../../helpers/functions";
import Success from "./Success";
import { uid } from "uid";
import Toolbar from "./Toolbar";
import { sendData } from "../../helpers/api";
import initItems from "./initItems";

const PopupForm = ({
  data,
  type = "book",
  setType,
  isFormSuccess,
  setFormSuccess,
}) => {
  const [toolbarOption, setToolbarOption] = useState("messages");
  const [formData, setFormData] = useState(null);
  const [items, setItems] = useState(null);
  const [additional, setAdditional] = useState({
    success: {
      title: {
        text: "Спасибо!\nВаша заявка принята.",
        italic: ["Ваша заявка принята."],
      },
      contactBack: "Мы свяжемся с вами в течение часа.",
    },
    permission:
      "Записываясь, вы даёте согласие\nна обработку персональных данных.",
    contactBackTel: "Перезвоним в течение получаса.",
    contactBackEmail: "Пришлём брошюру на вашу\nэлектронную почту",
    enterEmailOrNumber: "Введите телефон\nили электронную почту",
    contactBackBrochure: "Вышлем в SMS, WhatsApp или Telegram",
  });
  const phoneRef = useRef();
  const emailRef = useRef();

  useEffect(() => {
    if (!type) {
      setFormSuccess(false);
      setToolbarOption("messages");
    }
    hideScrollMain(type);
  }, [type, setFormSuccess]);

  useEffect(() => {
    const nItems = initItems;

    if (data) {
      Object.keys(data.items).map((item) => {
        nItems[item] = {
          ...nItems[item],
          ...data.items[item],
        };
      });

      setAdditional(data.additional);
    }
    setItems(nItems);
  }, [data]);

  useEffect(() => {
    if (items) {
      const nFormData = items.hasOwnProperty(type) ? items[type] : null;
      console.log("items", items);
      console.log("nFormData", nFormData);
      setFormData(nFormData);
    }
  }, [type]);

  const rqC = classNames({
    request: true,
    "request--active": type || isFormSuccess,
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    const sendingData = {
      type: type,
    };

    if (formData.inputs.includes("tel")) {
      sendingData.phone = phoneRef.current.value;
    }
    if (formData.inputs.includes("email")) {
      sendingData.email = emailRef.current.value;
    }

    if (type === "brochure2") {
      sendingData.phoneOption = toolbarOption;
    }

    sendData(sendingData);
    setFormSuccess(true);

    if (formData.inputs.includes("tel")) {
      phoneRef.current.value = null;
    }
    if (formData.inputs.includes("email")) {
      emailRef.current.value = null;
    }
  };

  const closeForm = () => {
    setType(null);
    setFormSuccess(false);
  };

  const rInputs = useMemo(() => {
    if (formData) {
      return formData.inputs.map((item) => {
        return item === "tel" ? (
          <input
            key={uid()}
            type="text"
            ref={phoneRef}
            className={`request__input`}
            placeholder={"+7 912 345-67-89"}
          />
        ) : (
          <input
            key={uid()}
            type="text"
            ref={emailRef}
            className={`request__input`}
            placeholder={"user@mail.ru"}
          />
        );
      });
    } else {
      return null;
    }
  }, [phoneRef, emailRef, formData, type]);
  return (
    <div className={rqC}>
      <img
        src="./images/icons/close-yellow.svg"
        alt="close"
        className="request__close"
        onClick={closeForm}
      />
      {isFormSuccess && <Success additional={additional} />}
      {formData && type && !isFormSuccess && (
        <>
          <div className="request__container">
            <div className="request__body">
              <p
                className="request__text"
                dangerouslySetInnerHTML={{
                  __html: addItalictToText(
                    formData.title.text,
                    formData.title.italic
                  ),
                }}
              />
              {formData.inputs.length > 1 && (
                <p
                  className="request__beforewords"
                  dangerouslySetInnerHTML={{
                    __html:
                      formData.type === "brochure2"
                        ? additional.contactBackBrochure
                        : additional.enterEmailOrNumber,
                  }}
                />
              )}
              <p className="request__beforewords"></p>
              {formData.inputs && (
                <div className={`request__group`}>{rInputs}</div>
              )}

              {formData.textUnder && (
                <p
                  className="request__afterwords"
                  dangerouslySetInnerHTML={{
                    __html:
                      formData.inputs[0] === "tel"
                        ? additional.contactBackTel
                        : additional.contactBackEmail,
                  }}
                />
              )}
              {type === "brochure2" && (
                <Toolbar
                  toolbarOption={toolbarOption}
                  setToolbarOption={setToolbarOption}
                />
              )}
            </div>
            <p
              className="request__text-bottom"
              dangerouslySetInnerHTML={{
                __html: additional.permission,
              }}
            />
            <button
              type="submit"
              className="request__button"
              onClick={handleSubmit}
            >
              {formData.button}
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default PopupForm;
