import "./index.scss";

import React from "react";
import { uid } from "uid";
import CustomButton from "../_common/CustomButton";

const Gallery = ({ data, setMainGalleryStartSlide, setType }) => {
  return (
    <div className="gallery-section">
      <section className="gallery">
        <h2 className="gallery__title">галерея</h2>
        <div className="masonry">
          {data.masonry.items.map((item, index) => {
            return (
              <div
                key={uid()}
                className={`masonry__item masonry__item--${index + 1}`}
              >
                <img
                  src={`./images/gallery/${item.image}.jpg`}
                  className={`masonry__image masonry__image--${index + 1}`}
                  alt={index}
                  onClick={() => setMainGalleryStartSlide(item.image)}
                />
                <p
                  className="masonry__text"
                  dangerouslySetInnerHTML={{
                    __html: item.text,
                  }}
                />
              </div>
            );
          })}
          <div
            className="more more--masonry"
            onClick={() => setMainGalleryStartSlide(0)}
          >
            <div className="more-number">55+</div>
            <div className="more-text">
              Все <br />
              фотографии
            </div>
          </div>
        </div>
        {data.button.active && (
          <CustomButton
            title={data.button.title}
            classes="gallery__button"
            func={() => setType(data.button.type)}
          />
        )}
      </section>
    </div>
  );
};

export default Gallery;
