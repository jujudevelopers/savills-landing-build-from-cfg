import "./index.scss";

import React from "react";
import classNames from "classnames";

const MoreButton = ({ title, amount, setAll, type, isAll }) => {
  const apClass = classNames({
    more: true,
    "more--apartments": type === "apartment",
    "more--hidden": isAll,
  });

  return (
    <div className={apClass} onClick={() => setAll(true)}>
      <div className="more-number">{amount}</div>
      <div
        className="more-text"
        dangerouslySetInnerHTML={{
          __html: title,
        }}
      />
    </div>
  );
};

export default MoreButton;
