import "./index.scss";

import React, { useEffect, useRef, useState } from "react";
import { uid } from "uid";
import classNames from "classnames";
import { Splide, SplideSlide } from "@splidejs/react-splide";

const PopupMainCarousel = ({
  data,
  mainGalleryStartSlide,
  setMainGalleryStartSlide,
}) => {
  const [startIndex, setStartIndex] = useState(0);

  const primaryRef = useRef(null);
  const secondaryRef = useRef(null);

  const images = {
    items: data.items,
    centeredImages: data.centeredImages,
  };

  useEffect(() => {
    primaryRef.current.sync(secondaryRef.current.splide);
  }, []);

  useEffect(() => {
    const index = images.items.findIndex(
      (item) => item === mainGalleryStartSlide
    );
    const startingIndex = index !== -1 ? index : 0;
    setStartIndex(startingIndex);
  }, [mainGalleryStartSlide]);

  const ACClass = classNames({
    "popup-main-carousel": true,
    "popup-main-carousel--active": mainGalleryStartSlide !== null,
  });

  const primaryOptions = {
    start: startIndex,
    type: "loop",
    width: 800,
    perPage: 1,
    perMove: 1,
    gap: 0,
    arrows: false,
    pagination: false,
  };

  const secondaryOptions = {
    start: startIndex,
    rewind: true,
    width: 800,
    gap: 16,
    cover: true,
    focus: "center",
    isNavigation: true,
    updateOnMove: true,
    fixedWidth: 90,
    fixedHeight: 90,
    arrows: false,
    pagination: false,
  };

  return (
    <div className={ACClass}>
      <img
        src="./images/icons/close-white.svg"
        alt="wind"
        className="popup-main-carousel__close"
        onClick={() => setMainGalleryStartSlide(null)}
      />
      <div className={`popup-main-carousel__slider--primary`}>
        <Splide options={primaryOptions} ref={primaryRef}>
          {images.items.map((item) => {
            const AIClass = classNames({
              "popup-main-carousel__image": true,
              "popup-main-carousel__image--full":
                !images.centeredImages.includes(item),
              "popup-main-carousel__image--center":
                images.centeredImages.includes(item),
            });
            return (
              <SplideSlide key={uid()}>
                <div className={`popup-main-carousel__image-wrap`}>
                  <img
                    className={AIClass}
                    src={`./images/gallery/${item}.jpg`}
                    alt={item}
                  />
                </div>
              </SplideSlide>
            );
          })}
        </Splide>
      </div>
      <div className={`popup-main-carousel__slider--secondary`}>
        <Splide options={secondaryOptions} ref={secondaryRef}>
          {images.items.map((item) => {
            const AIClass = classNames({
              "popup-main-carousel__image": true,
              "popup-main-carousel__image--full":
                !images.centeredImages.includes(item),
              "popup-main-carousel__image--center":
                images.centeredImages.includes(item),
            });
            return (
              <SplideSlide key={uid()}>
                <img
                  className={AIClass}
                  src={`./images/gallery/${item}.jpg`}
                  alt={item}
                />
              </SplideSlide>
            );
          })}
        </Splide>
      </div>
    </div>
  );
};

export default PopupMainCarousel;
