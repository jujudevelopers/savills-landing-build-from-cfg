import "./index.scss";

import React from "react";

const CustomButton = ({ title, classes, func }) => {
  return (
    <button type="button" className={`custom-button ${classes}`} onClick={func}>
      {title}
    </button>
  );
};

export default CustomButton;
