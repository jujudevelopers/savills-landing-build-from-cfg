import "./index.scss";

import React from "react";
import { phoneWithoutSymbols } from "../../helpers/functions";
import { uid } from "uid";

const Footer = ({ data }) => {
  return (
    <div className="footer-section">
      <footer className="footer">
        <p className="footer__address">
          {data.address}
          <br />
          {data.email}
        </p>
        <a
          href={`tel:+${phoneWithoutSymbols(data.phone)}`}
          className="footer__phone"
        >
          {data.phone}
        </a>
        <ul className="footer__list">
          {data.documents.map((item) => (
            <li key={uid()} className="footer__item">
              <a href={item.url} className="footer__resource" target="_blank">
                {item.name}
              </a>
            </li>
          ))}
        </ul>
      </footer>
    </div>
  );
};

export default Footer;
