import "./index.scss";

import React, { useState } from "react";
import { addItalictToText } from "../../helpers/functions";
import classNames from "classnames";

const About = ({ data }) => {
  const [isFull, setIsFull] = useState(false);
  const { title, text, red_title } = data.content;

  const atbC = classNames({
    "about__text-bg": true,
    "about__button--hide": isFull,
  });
  const abC = classNames({
    about__button: true,
    "about__button--hide": isFull,
  });
  const atxC = classNames({
    about__text: true,
    "about__text--active": isFull,
  });
  return (
      <div className="about-section">
        <section className="about">
          <h2
            className="about__title"
            dangerouslySetInnerHTML={{
              __html: addItalictToText(title.text, title.italic),
            }}
          />
          <div className={atxC}>
            <p className="about__text-tablet">{text}</p>
            <p className="about__text-mobile">{text}</p>
            <div className="about__line-left"></div>
            <div className={atbC}></div>
            <div className={abC}>
              <img
                src={"./images/icons/arrow-down.svg"}
                alt="arrow"
                onClick={() => setIsFull(true)}
              />
            </div>
          </div>
          <p className="about__subtitle">
            {red_title}
          </p>
          <img
            src={"./images/about/quotes.png"}
            alt="quotes"
            className="about__quotes"
          />
        </section>
      </div>
  );
};

export default About;
