const fs = require("fs");

const changeConfig = (newData) => {
  let data = JSON.stringify(newData);
  fs.writeFileSync("./src/_config/_final.json", data);
};

exports.changeConfig = changeConfig;
