const { exec } = require("child_process");

let prepareBuild = () => {
  return new Promise((resolve, rejects) => {
    exec("npm run build", (error, data, getter) => {
      if (error) {
        console.log("error", error.message);
        rejects();
      }
      if (getter) {
        console.log("data 1", data);
        return;
      }
      console.log("data 2", data);
      resolve();
    });
  });
};

exports.prepareBuild = prepareBuild;
