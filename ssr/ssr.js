import fs from "fs";
import path from "path";
import React from "react";
import ReactDOMServer from "react-dom/server";
import App from "../src/containers/App";

fs.readFile(path.resolve("./build/index.html"), "utf-8", (err, data) => {
  if (err) {
    console.log(err);
  }
  const content = data.replace(
    '<div id="root"></div>',
    `<div id="root">${ReactDOMServer.renderToString(<App />)}</div>`
  );

  fs.writeFile("./build/index.html", content, (err) => {
    if (err) {
      console.error(err);
      return;
    }
  });
});
