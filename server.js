const express = require("express");
const app = express();
const fs = require("fs");
const cors = require("cors");
const path = require("path");
const { changeConfig } = require("./server/changeConfig");
const { prepareBuild } = require("./server/prepBuild");
const { zipper } = require("./zip");

app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname, "out")));

app.get("/", function (req, res) {
  fs.readFile("./builder.html", (err, html) => {
    if (err) res.write("Error");
    else res.write(html);
    res.end();
  });
});

app.get("/builder/start-config", function (req, res) {
  let rawdata = fs.readFileSync(`./src/_config/_start.json`);
  let config = JSON.parse(rawdata);
  req.body.data = config;
  res.send(req.body);
});

app.post("/builder", async function (req, res) {
  changeConfig(req.body.config);

  const prepData = req.body;
  const configVersion = prepData.config.version;

  res.writeHead(200);
  let timer = setInterval(() => {
    res.write(' ');
  }, 5000);
  
  await prepareBuild();
  
 const zipperData = await zipper(configVersion, req, res);
  res.write(JSON.stringify(zipperData));
  clearInterval(timer);
  res.end();

});

const port = process.env.PORT || "6656";
app.listen(port);
console.log("Server started! At http://localhost:" + port);
